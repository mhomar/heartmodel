from django.urls import path
from . import views
urlpatterns = [
    path('', views.index, name='index'),
    path('/mlmodel',views.mlmodel,name='mlmodel'),
    path('/mlmodel_qs',views.mlmodel_qs,name="mlmodel_qs")
]