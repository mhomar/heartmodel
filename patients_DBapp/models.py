from django.db import models
import datetime
from django_pandas.managers import DataFrameManager

class Patient_basic_record(models.Model):
    #male=models.BooleanField(choices=[(1,'Male'),(0,'Female')])
    date_sent = models.DateTimeField(auto_now_add=True,editable=False,null=True)
    Patient_age=models.IntegerField(max_length=10,)
    Current_Smoker=models.BooleanField(default=False)
    CigsPerDay=models.IntegerField(max_length=10)
    BPMeds=models.BooleanField()
    PrevalentStroke=models.IntegerField(max_length=10)
    PrevalentHyp=models.IntegerField(max_length=10)
    diabetes=models.BooleanField()
    totChol=models.IntegerField(max_length=10)
    sysBP=models.IntegerField(max_length=10)
    diaBP=models.IntegerField(max_length=10)
    BMI=models.FloatField(max_length=10)
    heartRate=models.IntegerField(max_length=10)
    glucose=models.IntegerField(max_length=10)
    TenYearCHD=models.BooleanField()
    objects = DataFrameManager()
    #def __str__(self):
        #return self.name
    class Meta :
        ordering = ['date_sent'] # records for each patient are ordrered by date 



