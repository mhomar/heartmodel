# Generated by Django 2.2.6 on 2019-11-18 14:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('patients_DBapp', '0005_auto_20191118_1350'),
    ]

    operations = [
        migrations.AddField(
            model_name='patient_basic_record',
            name='Current_Smoker',
            field=models.BooleanField(default=False),
        ),
    ]
