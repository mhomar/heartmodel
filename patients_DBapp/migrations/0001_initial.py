# Generated by Django 2.2.6 on 2019-11-18 13:15

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Book',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=200, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Patient',
            fields=[
                ('patient_ID', models.AutoField(primary_key=True, serialize=False)),
                ('male', models.BooleanField(choices=[(1, 'Male'), (0, 'Female')])),
                ('Current_Smoker', models.BooleanField()),
                ('BPMeds', models.BooleanField()),
                ('PrevalentStroke', models.BooleanField()),
                ('PrevalentHyp', models.BooleanField()),
                ('diabetes', models.BooleanField()),
                ('Patient_age', models.IntegerField()),
                ('CigsPerDay', models.IntegerField()),
                ('totChol', models.IntegerField()),
                ('sysBP', models.IntegerField()),
                ('diaBP', models.IntegerField()),
                ('BMI', models.IntegerField()),
                ('heartRate', models.IntegerField()),
                ('glucose', models.IntegerField()),
                ('TenYearCHD', models.BooleanField()),
            ],
        ),
        migrations.CreateModel(
            name='PatientRecord',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('male', models.BooleanField(choices=[(1, 'Male'), (0, 'Female')])),
                ('Current_Smoker', models.BooleanField()),
                ('BPMeds', models.BooleanField()),
                ('PrevalentStroke', models.BooleanField()),
                ('PrevalentHyp', models.BooleanField()),
                ('diabetes', models.BooleanField()),
                ('Patient_age', models.IntegerField()),
                ('CigsPerDay', models.IntegerField()),
                ('totChol', models.IntegerField()),
                ('sysBP', models.IntegerField()),
                ('diaBP', models.IntegerField()),
                ('BMI', models.IntegerField()),
                ('heartRate', models.IntegerField()),
                ('glucose', models.IntegerField()),
                ('TenYearCHD', models.BooleanField()),
            ],
        ),
    ]
