from django.contrib import admin
from .models import *
from import_export import resources
from patients_DBapp.models import *
from import_export.admin import ImportExportModelAdmin


class patientResource(resources.ModelResource):

    class Meta:
        model = Patient_basic_record
        #fields=('id','male','Patient_age','Current_Smoker','CigsPerDay','BPMeds','PrevalentStroke','PrevalentHyp','diabetes','totChol','sysBP','diaBP','BMI','heartRate','glucose','TenYearCHD')
        #skip_unchanged = True
        #report_skipped = True
        # exclude = ('id',)

class patientAdmin(ImportExportModelAdmin):
    resource_class = patientResource


# Register your models here.
admin.site.register(Patient_basic_record, patientAdmin)
# admin.site.register(Book)
# admin.site.register(Author)