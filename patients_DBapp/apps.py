from django.apps import AppConfig


class PatientsDbappConfig(AppConfig):
    name = 'patients_DBapp'
