from django.shortcuts import render
from django.http import HttpResponse
from .models import *
import sys, os
sys.path.append(os.path.abspath(os.path.join('..', 'common')))
from common.HSmodel import *

def index(request):
    return HttpResponse("you got a response ")
def mlmodel(request):
    time_file=Heart_stroke_classifier()

    return render(request, 'mlmodel.html',{'time_file':time_file})
def mlmodel_qs(request):
    time_query=Heart_stroke_classifier_qs()
    return render(request, 'mlmodel_qs.html',{'time_query':time_query})