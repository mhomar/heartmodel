import json
from django_pandas.io import read_frame
import pandas as pd
from pandas.io.json import json_normalize
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import confusion_matrix,accuracy_score
from sklearn.model_selection import train_test_split
from json import JSONDecoder
from collections import OrderedDict
from patients_DBapp.models import Patient_basic_record
from Heart_Model.models import Patient_Classified
from django.db import connection
from time import process_time
from django.forms.models import model_to_dict
# def Heart_stroke_classifier():
#     #data preprocessing code block
#     # t1_start = process_time()  

#     with open('Framingham.json', 'r') as input_file:
#          input_data = input_file.read()
#     data_content = json.loads(input_data, object_pairs_hook=OrderedDict)
#     heart_df=json_normalize(data_content)
#     heart_df= pd.DataFrame(heart_df)

#     #qs=Patient_basic_record.objects.all()
#     #heart_df=qs.to_dataframe(index="id")
#     #heart_df.dropna(axis=0,inplace=True)

#     #heart_df.drop(['education'],axis=1,inplace=True)
#     #null values removal 
    

#     X = heart_df.iloc[:,:-1].values
#     y = heart_df.iloc[:, -1].values 
#     x_train, x_test, y_train, y_test = train_test_split(X ,y,test_size=0.1,random_state=0)
#     rfc= RandomForestClassifier(random_state=0)
#     rfc.fit(x_train,y_train)
#     y_pred=rfc.predict(x_test)
#     score=accuracy_score(y_test,y_pred)
#     # t1_stop = process_time() 
#     # time_elapsed_file=t1_stop-t1_start
#     return(score)
def Heart_stroke_classifier_qs():
    #data preprocessing code block
    # t1_start = process_time()  
    qs=Patient_basic_record.objects.all()
    heart_df=qs.to_dataframe(index="id")
    
    y = Patient_Classified.objects.all().to_dataframe()
    y=y.iloc[:,1].values
    rows_X=heart_df.shape[0]-1  
    rows_y=y.shape[0] #number of records labeled with 10YEARSCHD
    X = heart_df.iloc[:rows_y,:].values #ONLY USE LABELED RECORDS TO TRAIN AND TEST 
    
    x_train, x_test, y_train, y_test = train_test_split(X ,y,test_size=0.1,random_state=0)
    rfc= RandomForestClassifier(random_state=0)
    rfc.fit(x_train,y_train)
    y_pred=rfc.predict(x_test)
    score=accuracy_score(y_test,y_pred)
    
    while rows_X+1>rows_y:
        X_new=Patient_basic_record.objects.all()[rows_X:rows_X+1]
        X_new=X_new.to_dataframe(index="id")
        new_pred=rfc.predict(X_new)
        New_Patient_Classified=Patient_Classified.objects.create(id=rows_X,TenYearCHD=new_pred,Patient_ID=Patient_basic_record.objects.all()[rows_X:rows_X+1][0])
        rows_X-=1
    last_record = Patient_basic_record.objects.filter(Patient_basic_record_id=id).last()

    return("Average accuracy of the model ",last_record)

    #t1_stop = process_time() 
    #time_elapsed_query=t1_stop-t1_start    

