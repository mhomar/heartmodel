from django.db import models
from patients_DBapp.models import Patient_basic_record
from django_pandas.managers import DataFrameManager

# Create your models here.
class Patient_Classified(models.Model):
    TenYearCHD=models.BooleanField()
    Patient_Records=models.ManyToManyField(Patient_basic_record,null=True)
    objects = DataFrameManager()
