from django.contrib import admin
from .models import Patient_Classified
from import_export import resources
from import_export.admin import ImportExportModelAdmin



class patientClassResource(resources.ModelResource):

    class Meta:
        model = Patient_Classified

class patientClassAdmin(ImportExportModelAdmin):
    resource_class = patientClassResource

admin.site.register(Patient_Classified,patientClassAdmin)